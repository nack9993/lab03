import { TestBed, inject } from '@angular/core/testing';

import { StudentsFlieImplService } from './students-flie-impl.service';

describe('StudentsFlieImplService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [StudentsFlieImplService]
    });
  });

  it('should be created', inject([StudentsFlieImplService], (service: StudentsFlieImplService) => {
    expect(service).toBeTruthy();
  }));
});
