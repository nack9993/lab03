import { TestBed, inject } from '@angular/core/testing';

import { StudentsPeopleImplService } from './students-people-impl.service';

describe('StudentsPeopleImplService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [StudentsPeopleImplService]
    });
  });

  it('should be created', inject([StudentsPeopleImplService], (service: StudentsPeopleImplService) => {
    expect(service).toBeTruthy();
  }));
});
